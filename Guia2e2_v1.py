#!/usr/bin/python
# -*- coding: utf-8 -*-


# funcion para pasar los datos requeridos a lista
def pasar_lista(covid):
    archivo = []
    paises = []
    infectados = []
    for i, linea in enumerate(covid):
        lista = linea.replace('.0', '').split(',')
        if i > 0:
            if lista[1] == '03/27/2020':
                # Relleno las listas vacias con las columnas que necesito
                paises.append(lista[3])
                archivo.append(linea)
                infectados.append(lista[5])
        for i in range(len(archivo)):
            print(paises[i] + "", infectados[i])
    print("\n Paises contagiados: ", len(set(paises)))

# funcion main que llamara a las demas funciones
if __name__ == '__main__':

    covid = open('covid_19_data.csv')
    pasar_lista(covid)
    covid.close()