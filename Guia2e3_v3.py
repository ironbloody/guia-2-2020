#!/usr/bin/python
# -*- coding: utf-8 -*-

import json

# funcion para guardar el json


def guardar(dic):
    with open("aa.json", 'w') as file:
        json.dump(dic, file)

# funcion para cargar el json


def cargar():
    with open("aa.json", 'r') as file:
        dic = json.load(file)
    return dic

# funcion para insertar los datos en el json


def insercion():
    dic = cargar()
    amino = {input("ingrese el aminoacido"): input("ingrese la formula")}
    dic.update(amino)
    guardar(dic)
    print(dic)

# funcion para buscar


def busqueda():
    dic = cargar()
    # Para recorrer el json
    for i in range(len(dic)):
        buscar = input("Ingrese el aminoacido a buscar: ")
        if buscar in dic:
            print("Se encuentra")
            print(dic)
        else:
            print("No se encuentra")

# funcion para editar un aminoacido junto con su formula


def editar():
    dic = cargar()
    print(dic)
    edit = input("ingrese el aminoacido a editar")
    # si el aminoacido se encuentra en el archivo se procedera a editarlo
    if edit in dic:
        amino = input("ingrese el nuevo aminoacido")
        formula = input("ingrese la nueva formula")
        del dic[edit]
        dic[amino] = formula
        print(dic)
        guardar(dic)
    else:
        print("Este aminoacido no existe en el archivo")


# funcion para eliminar un aminoacido con su respectiva formula


def eliminar():
    dic = cargar()
    delete = input("ingrese el aminoacido que desea eliminar: ")
    # si el aminoacido se encentra en el archivo se eliminara
    if delete in dic:
        del dic[delete]
        guardar(dic)
        print(dic)

# menu que llamara a las otras funciones


def menu():
    while True:
        print("1 para insertar")
        print("2 para buscar")
        print("3 para editar")
        print("4 para eliminar")
        print("5 para salir")
        opcion = input("Ingrese una opción: ")

        if opcion == "1":
            insercion()

        elif opcion == "2":
            busqueda()

        elif opcion == "3":
            editar()

        elif opcion == "4":
            eliminar()

        elif opcion == "5":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass

# Main que llamara al menu el cual llamara a las otras funciones
if __name__ == "__main__":

    menu()
