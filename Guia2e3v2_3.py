#!/usr/bin/python
# -*- coding: utf-8 -*-

import json

aminoacido = {"histidine": "C6H9N3O2", "isoleucine": "C6H13NO2",
              "leucine": "C6H13NO2", "lysine": "C6H14N2O2",
              "methionine ": "C5H11NO2S", "phenylalanine": "C9H11NO2",
              "threonine": "C4H9NO3", "tryptophan": "C11H12N2O2",
              "valine": "C5H11NO2", "alanine": "C3H7NO2",
              "glutamic acid": "C5H9NO4", "aspartic acid": "C4H7NO4",
              "asparagine": "C4H8N2O3", "arginine": "C6H14N4O2",
              "cysteine": "C3H7NO2S", "glutamine": "C5H10N2O3",
              "tyrosine": "C9H11NO3", "glycine": "C2H5NO2",
              "proline": "C5H9NO2", "serine": "C3H7NO3"}

# funcion para guardar el json


def guardar(dic):
    with open("aa.json", 'w') as file:
        json.dump(dic, file)

# funcion para cargar el json


def cargar():
    with open("aa.json", 'r') as file:
        dic = json.load(file)
    return dic

# funcion para insertar los datos en el json


def insercion():
    dic = cargar()
    amino = {input("ingrese el aminoacido"): input("ingrese la formula")}
    dic.update(amino)
    guardar(dic)
    print(dic)

# funcion para buscar


def busqueda():
    dic = cargar()
    # Para recorrer el json
    for i in range(len(dic)):
        buscar = input("Ingrese el aminoacido a buscar: ")
        if buscar in dic:
            print("Se encuentra")
            print(dic)
        else:
            print("No se encuentra")

# funcion para editar


def editar():
    dic = cargar()
    edit = input("ingrese el aminoacido a editar")
    if edit in dic:
        x = print("ingrese el nuevo aminoacido")
        guardar()

# funcion par eliminar


def eliminar():
    dic = cargar()
    delete = input("ingrese el aminoacido que desea eliminar: ")

    if delete in dic:
        del dic[delete]
        guardar(dic)
        print(dic)

# menu


def menu():
    while True:
        print("1 para insertar")
        print("2 para buscar")
        print("3 para editar")
        print("4 para eliminar")
        print("5 para salir")
        opcion = input("Ingrese una opción: ")

        if opcion == "1":
            insercion()

        elif opcion == "2":
            busqueda()

        elif opcion == "3":
            editar()

        elif opcion == "4":
            eliminar()

        elif opcion == "5":
            quit()

        else:
            print("La opción ingreseda no es válida, intente nuevamente")
            pass

# Main que llamara al menu el cual llamara a las demas funciones
if __name__ == "__main__":

    menu()
